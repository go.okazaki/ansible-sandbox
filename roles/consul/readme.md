# Consul

## Setup ACL

- Create Bootstrap (Master) Token
```
$ curl -i -XPUT 127.0.0.1:8500/v1/acl/bootstrap
```

- Create Agent Token
```
$ curl -i -XPUT 127.0.0.1:8500/v1/acl/create -H 'X-Consul-Token: ${master_token}' \
-d '{
  "Name" : "Agent Token",
  "Type" : "client",
  "Rules" : "key \"\" { policy = \"write\" } node \"\" { policy = \"write\" } service \"\" { policy = \"read\" }"
}'
```
- Update Anonymous ACL
```
$ curl -i -XPUT 127.0.0.1:8500/v1/acl/update -H 'X-Consul-Token: ${master_token}' \
-d '{
  "ID": "anonymous",
  "Name" : "Anonymous Token",
  "Type" : "client",
  "Rules" : "key \"\" { policy = \"deny\" } node \"\" { policy = \"deny\" } service \"consul\" { policy = \"deny\" }"
}'
```
