#!/usr/bin/env bash

apt-get install -y python-minimal

update-rc.d -f chef-client remove
update-rc.d -f puppet remove
